import unittest

import pandas as pd
from utils.utility_functions import remove_dollar


class TransformTest(unittest.TestCase):

    def createTestData(self):
        col1 = ['$2.34', '4.45', '$5.2', '8.194', '$9.24']
        data = pd.DataFrame({'x1': col1})

        return data

    def expectedData(self):
        exp = [2.34, 4.45, 5.2, 8.194, 9.24]
        expected = pd.DataFrame({'x1': exp})

        return expected

    def test_utils(self):
        data = self.createTestData()
        expected = self.expectedData()

        data['x1'] = data['x1'].apply(remove_dollar)

        pd.testing.assert_frame_equal(data, expected)


if __name__ == '__main__':
    unittest.main()
