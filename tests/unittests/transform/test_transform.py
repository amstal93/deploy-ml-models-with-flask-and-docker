import unittest

import pandas as pd
from transform.transform import Transform


class TransformTest(unittest.TestCase):

    def createTestData(self):
        col1 = [1, 3, 5, 73, 15, 931]
        col2 = ['Jason', 'Rob', 'Ellen', 'Emily', 'Tyler', 'Raja']
        data = pd.DataFrame({'x1': col1, 'x2': col2})

        return data

    def expectedData(self):
        expected_col1 = [1, 3, 5, 73, 15, 931]
        expected_col2 = [2, 4, 0, 1, 5, 3]
        expected = pd.DataFrame({'x1': expected_col1, 'x2': expected_col2})

        return expected

    def test_transform(self):
        data = self.createTestData()
        expected = self.expectedData()

        trns = Transform()
        data = trns.categorical_transformation(data)

        pd.testing.assert_frame_equal(data, expected)


if __name__ == '__main__':
    unittest.main()
