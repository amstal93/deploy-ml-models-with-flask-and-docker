import unittest

import numpy as np
from model.model import CustomModel
from sklearn.model_selection import train_test_split


class TransformTest(unittest.TestCase):

    def createTestData(self):
        X, y = np.arange(10).reshape((5, 2)), range(5)

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

        return X_train, X_test, y_train, y_test

    def test_model(self):
        X_train, X_test, y_train, y_test = self.createTestData()

        cls = CustomModel(n_estimators=10, max_depth=2)
        estimator = cls.fit(X_train, y_train)
        train_y_pred = estimator.predict(X_train)

        np.testing.assert_array_equal(train_y_pred.shape, 3)


if __name__ == '__main__':
    unittest.main()
