#! /bin/bash
echo "Starting up flask application..."
app="docapp:v1"
docker build -t ${app} .
docker container run -d -p 5001:5001 ${app}
echo "Container built. Check status with 'docker ps -a'"